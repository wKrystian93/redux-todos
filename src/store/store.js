import {createStore} from "redux";
import {addTodoReducer, deleteTodoReducer, selectCompleteReducer} from "../reducers";

const initialState = {
    todos:[]
}

const rootReducer = (state=initialState, action) => {
    switch (action.type) {
        case 'ADD_TODO': return addTodoReducer(state, action);

        case 'DELETE_TODO': return deleteTodoReducer(state,action);

        case 'SELECT_COMPLETE': return selectCompleteReducer(state,action);
        default: return state;
    }
}

export const store = createStore(rootReducer,window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());