import React from 'react';
import ListOfTodos from "./components/ListOfTodos";
import TodoForm from "./components/TodoForm";
function App() {
  return (
        <div className="App container">
            <TodoForm/>
            <ListOfTodos />
        </div>
  );
}

export default App;
