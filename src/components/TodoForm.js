import React, {useState} from "react";
import {useDispatch} from 'react-redux';

const TodoForm = () => {

    const [todo,setTodo] = useState({title:''});
    const dispatch = useDispatch();

    const addTodo = (e) => {
        if(todo.title === "") return;

        dispatch({type:'ADD_TODO',title:todo.title})
        setTodo({title: ''})
    }

    const onChangeInput = (e) =>{
        setTodo({title: e.target.value});
    }

    return (
        <div>
            <div className="input-group mb-3">
                <input
                    onChange={onChangeInput}
                    onKeyDown={(e) => e.key === 'Enter' ? addTodo() : null}
                    type="text"
                    className="form-control"
                    placeholder=""
                    aria-label=""
                    aria-describedby="basic-addon1"
                    value={todo.title}
                />
                <div className="input-group-prepend">
                    <button
                        onClick={(e)=>addTodo(e)}
                        className="btn btn-outline-secondary"
                        type="button">Add Todo
                    </button>
                </div>
            </div>

        </div>
    );
};

export default TodoForm;