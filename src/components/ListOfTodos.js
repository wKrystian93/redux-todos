import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import  '../App.css'

const ListOfTodos = () => {

    const todos = useSelector(state => state.todos);
    const dispatch = useDispatch();

    const deleteTodo = (id) =>{
        dispatch({type:'DELETE_TODO',id:id})
    };

    const selectComplete = (e, id) =>{
        e.stopPropagation();

        dispatch({type:'SELECT_COMPLETE',id:id})
    }

    return (
        <ul className="list-group">
            {todos.map(todo=>(
                    <li className={`list-group-item d-flex align-items-center flex-row justify-content-between ${todo.complete ? 'strike' : null}`} onClick={() => deleteTodo(todo.id)} key={todo.id}>
                        {todo.title}
                        <input onClick={(e) => selectComplete(e,todo.id)} type="checkbox"/>
                    </li>
                ))}
        </ul>
    );
};

export default ListOfTodos;