import uuidv from "uuid";

export const addTodoReducer = (state, action) =>{
    return{
        ...state,
        todos:state.todos.concat(
            {
                id:uuidv(),
                title:action.title,
                complete:false
            }
        ),
    }
}

