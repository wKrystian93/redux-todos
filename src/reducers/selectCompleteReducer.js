

export const selectCompleteReducer = (state, action) =>{
    return{
        ...state,
        todos: state.todos.map(todo=>{
            if(todo.id === action.id){
                todo.complete = !todo.complete;
            }
            return todo
        })
    }
}

