export const deleteTodoReducer = (state, action) =>{
    return{
        ...state,
        todos: state.todos.filter(todo=>todo.id!==action.id),
    }
}

